import sys
import random

# Neuron chain model
# Remembers what it has seen and transforms input based on history
# History is overwritten over time
# No preference for similarities (as we would expect to see in neurons)
# Constant memory
# First rows change faster than last rows (by design)

def train(inputs, q):
    for v in inputs:
        cur = v
        for i in xrange(len(q)):
            p_change = [(a - b)/100. for a, b in zip(cur, q[i])]
            s = [v + p * v for v, p in zip(q[i], p_change)]
            s = [v / sum(s) * 100. for v in s]


            #print 'x', p_change
            #print 'hi', cur, s, q[i]
            assert 99 <= sum(s) <= 101
            q[i] = s
            cur = s
        yield cur, q

def run_inputs(inputs, q):
    print '\nstarting'
    gen = train(inputs, q)
    for out, l in gen:
        print '------'
        print 'output', out
        print '\n'.join(str(s) for s in l)

if __name__ == "__main__":
    inputs = [[90, 1, 9]] * 40
    q = [[33, 33, 34]] * 5

    run_inputs(inputs, q)

    inputs2 = [[5, 45, 50]] * 5
    run_inputs(inputs2, q)
